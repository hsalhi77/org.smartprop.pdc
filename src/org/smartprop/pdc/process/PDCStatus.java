package org.smartprop.core.process;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.logging.Level;
import org.adempiere.exceptions.AdempiereException;
import org.smartprop.core.model.MPayment;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.AdempiereUserError;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Msg;

/**
 *	Create Listing Request
 *	
 *  @author Habib Salhi
 */
public class PDCStatus extends SvrProcess
{
	/** Request					*/
	private int	m_C_Payment_ID = 0;
	private int AD_PInstance_ID=0;
	private String p_Pdc_Action = null;
	
	/**
	 * 	Prepare
	 */
	protected void prepare ()
	{
		AD_PInstance_ID = getAD_PInstance_ID();
		ProcessInfoParameter[] para = getParameter();
		for (int i = 0; i < para.length; i++)
		{
			String name = para[i].getParameterName();
			if (para[i].getParameter() == null)
				;
			else if (name.equals("Pdc_Status"))
				p_Pdc_Action = para[i].getParameterAsString();
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + name);
		}
	}	//	prepare

	/**
	 * 	Process It
	 *	@return message
	 *	@throws Exception
	 */
	protected String doIt () throws Exception
	{
		StringBuilder msgreturn = null;
		//first check if there is action is enforceable
		String pdc_status = DB.getSQLValueString(get_TrxName(), "SELECT distinct(pdc_status) FROM C_Payment WHERE C_Payment_ID = "
				+ "(Select max(T_Selection_ID) from T_Selection where AD_PInstance_ID = ? )",AD_PInstance_ID);
		log.warning("pdc_status = " + pdc_status + " p_Pdc_Action="+p_Pdc_Action);
		//Maturing Cheques under collection
		if (pdc_status.equals("U") && p_Pdc_Action.equals("B"))
			throw new AdempiereUserError(Msg.getMsg(getCtx(), "Cannot Bounce PDC Under Collection"));
		else if (pdc_status.equals("B") && (p_Pdc_Action.equals("T") || p_Pdc_Action.equals("C")))
			throw new AdempiereUserError(Msg.getMsg(getCtx(), "Cannot Transfer or Withdraw a Bounced PDC"));
		else {
			//now process the chosen pdc's
			String sql = "Select T_Selection_ID from T_Selection where AD_PInstance_ID = ? ";
			PreparedStatement pstmt = null;
			ResultSet rs = null;
			try {
				pstmt = DB.prepareStatement(sql.toString(), get_TrxName());
				pstmt.setInt(1, AD_PInstance_ID);
				rs = pstmt.executeQuery();
				while (rs.next()) {
					m_C_Payment_ID = rs.getInt("T_Selection_ID");
					//now lets get the chosen action
					if(p_Pdc_Action.equals("T"))
					{
						MPayment oldPayment = new MPayment(getCtx(), m_C_Payment_ID, get_TrxName());
						oldPayment.setPdc_Status("T");
						oldPayment.save();
					} else if(p_Pdc_Action.equals("C"))
					{
						MPayment oldPayment = new MPayment(getCtx(), m_C_Payment_ID, get_TrxName());
						oldPayment.setPdc_Status("C");
						oldPayment.setIsReconciled(true);
						oldPayment.save();
						oldPayment.reverseCorrectIt();
						MPayment newPayment = new MPayment(getCtx(), oldPayment.getReversal_ID(), get_TrxName());
						newPayment.setPdc_Status("C");
						newPayment.setIsReconciled(true);
						newPayment.save();
					} else if(p_Pdc_Action.equals("B"))
					{
						MPayment oldPayment = new MPayment(getCtx(), m_C_Payment_ID, get_TrxName());
						oldPayment.setPdc_Status("B");
						oldPayment.setPdc_Amt_Outstanding(oldPayment.getPayAmt());
						oldPayment.setPdc_Paid_Amt(Env.ZERO);
						oldPayment.save();
						oldPayment.reverseCorrectIt();
						MPayment newPayment = new MPayment(getCtx(), oldPayment.getReversal_ID(), get_TrxName());
						newPayment.setPdc_Status("B");
						newPayment.save();
					}
					
					log.warning(" Payment chosen " + m_C_Payment_ID + " Action Chosen" + p_Pdc_Action);

					
					
					
					
				}
			} catch (Exception e) {
				throw new AdempiereException(e);
			} finally {
				DB.close(rs, pstmt);
				rs = null;
				pstmt = null;
			}
		}//close else
		msgreturn = new StringBuilder("@Action@ = ").append(p_Pdc_Action);
		return msgreturn.toString();
	}// close do-it
	
}
