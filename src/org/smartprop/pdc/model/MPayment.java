/******************************************************************************
 * Product: iDempiere Free ERP Project based on Compiere (2006)               *
 * Copyright (C) 2014 Redhuan D. Oon All Rights Reserved.                     *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 *  FOR NON-COMMERCIAL DEVELOPER USE ONLY                                     *
 *  @author Redhuan D. Oon  - red1@red1.org  www.red1.org                     *
 *****************************************************************************/

package org.smartprop.core.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;
import java.util.logging.Level;

import org.adempiere.model.POWrapper;
import org.compiere.model.MOrder;
import org.compiere.model.MOrderLine;
import org.compiere.model.Query;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;


public class MPayment extends org.compiere.model.MPayment {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final String COLUMNNAME_Pdc_Status = "pdc_status";
	public static final String COLUMNNAME_Pdc_Paid_Amt = "pdc_paid_amt";
	public static final String COLUMNNAME_Pdc_Amt_Outstanding = "pdc_amt_outstanding";
	
	CLogger log = CLogger.getCLogger(MOrder.class);

	public MPayment(Properties ctx, int C_Payment_ID, String trxName) {
		super(ctx, C_Payment_ID, trxName); 

	}

	public MPayment(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
	}
	
	private void doit() {
		// TODO Auto-generated method stub
		
		
	}

	/**
	 * Get Pdc_Status.
	 * 
	 * @return Pdc_Status
	 */
	public String getPdc_Status() {
		return (String) get_Value(COLUMNNAME_Pdc_Status);
	}
	
	/**
	 * Set Pdc_Status.
	 * 
	 * @param Pdc_Status
	 *            
	 */
	public void setPdc_Status (String Pdc_Status)
	{
		set_Value (COLUMNNAME_Pdc_Status, Pdc_Status);
	}
	
	/**
	 * Get Pdc_Paid_Amt.
	 * 
	 * @return Pdc_Paid_Amt
	 */
	public BigDecimal getPdc_Paid_Amt() {
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Pdc_Paid_Amt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}
	
	/**
	 * Set Pdc_Paid_Amt.
	 * 
	 * @param Pdc_Paid_Amt
	 *            
	 */
	public void setPdc_Paid_Amt (BigDecimal Pdc_Paid_Amt)
	{
		set_Value (COLUMNNAME_Pdc_Paid_Amt, Pdc_Paid_Amt);
	}
	
	/**
	 * Get Pdc_Amt_Outstanding.
	 * 
	 * @return Pdc_Amt_Outstanding
	 */
	public BigDecimal getPdc_Amt_Outstanding() {
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Pdc_Amt_Outstanding);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}
	
	/**
	 * Set Pdc_Amt_Outstanding.
	 * 
	 * @param Pdc_Amt_Outstanding
	 *            Order
	 */
	public void setPdc_Amt_Outstanding (BigDecimal Pdc_Amt_Outstanding)
	{
		set_Value (COLUMNNAME_Pdc_Amt_Outstanding, Pdc_Amt_Outstanding);
	}

	
}
