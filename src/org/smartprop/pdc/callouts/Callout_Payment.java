package org.smartprop.pdc.callouts;
import java.util.Properties;
import java.util.logging.Level;
import org.adempiere.base.IColumnCallout;
import org.compiere.model.GridField;
import org.compiere.model.GridTab;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.smartprop.pdc.model.MPayment;


public class Callout_Payment implements IColumnCallout {

	CLogger log = CLogger.getCLogger("Callout_Payment.class");
	
	@Override
	public String start(Properties ctx, int WindowNo, GridTab mTab,
			GridField mField, Object value, Object oldValue) {
	
    //System.out.println(ctx + " -WindowNO- " + WindowNo + " -mTab- " + mTab + " -mField- " + mField + " - " + value + " - " + oldValue);
	if (value != oldValue) {
		
		if(mField.getColumnName().equals("Ref_Payment_ID"))
		{
		//log.warning("==callout== Ref_Payment_ID = " + (Integer)mTab.getValue("Ref_Payment_ID"));
		MPayment oldPayment = new MPayment(Env.getCtx(), (Integer)mTab.getValue("Ref_Payment_ID"), null);
		mTab.setValue(MPayment.COLUMNNAME_Pdc_Amt_Outstanding, oldPayment.getPdc_Amt_Outstanding());
		mTab.setValue(MPayment.COLUMNNAME_PayAmt, oldPayment.getPdc_Amt_Outstanding());
		} 
	}
	return "";
	
	}
}
