package org.smartprop.pdc.factories;

import java.util.ArrayList;
import java.util.List;

import org.adempiere.base.IColumnCallout;
import org.adempiere.base.IColumnCalloutFactory;
import org.compiere.util.CLogger;
import org.smartprop.pdc.model.MPayment;
import org.smartprop.pdc.callouts.*;

public class PluginColumnCalloutFactory implements IColumnCalloutFactory{
	CLogger log = CLogger.getCLogger("PluginColumnCalloutFactory.class");

	@Override
	public IColumnCallout[] getColumnCallouts(String tableName,
			String columnName) {
		//log.warning(" -- callout factory -- tableName " + tableName.toString() + " columnname = " + columnName.toString());
		
		// TODO Auto-generated method stub
		List<IColumnCallout> list = new ArrayList<IColumnCallout>();
		
		if(tableName.equalsIgnoreCase(MPayment.Table_Name) && columnName.equalsIgnoreCase(MPayment.COLUMNNAME_Ref_Payment_ID))
			list.add(new Callout_Payment());
		
		

		return list != null ? list.toArray(new IColumnCallout[0]) : new IColumnCallout[0];
	}

}
