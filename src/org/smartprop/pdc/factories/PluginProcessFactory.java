package org.smartprop.pdc.factories;

import org.adempiere.base.IProcessFactory;
import org.compiere.process.ProcessCall;
import org.smartprop.pdc.process.PDCStatus;

public class PluginProcessFactory implements IProcessFactory {

	@Override
	public ProcessCall newProcessInstance(String className) {

		System.out.println(" ---PluginProcessFactory loading---"+className.toString());

		if (className.equalsIgnoreCase("org.smartprop.core.process.PDCStatus"))
			return new PDCStatus();

		return null;
	}

}
