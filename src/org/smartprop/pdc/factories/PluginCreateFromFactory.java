package org.smartprop.pdc.factories;

import org.compiere.grid.ICreateFrom;
import org.compiere.grid.ICreateFromFactory;
import org.compiere.model.GridTab;
import org.compiere.model.MBankStatement;
import org.compiere.util.CLogger;
import org.smartprop.pdc.grid.WCreateFromBStatementUI;


/**
 * 
 * @author Habib Salhi
 *
 */
public class PluginCreateFromFactory implements ICreateFromFactory 
{
	CLogger log = CLogger.getCLogger("PluginCreateFromFactory.class");
	@Override
	public ICreateFrom create(GridTab mTab) 
	{
		String tableName = mTab.getTableName();
		log.warning(" -- PluginCreateFromFactory factory -- tableName " + tableName.toString());
		if (tableName.equals(MBankStatement.Table_Name))
			return new WCreateFromBStatementUI(mTab);
		return null;
	}

}