-- I forgot to set the DICTIONARY_ID_COMMENTS System Configurator
-- Oct 14, 2017 1:02:28 PM GST
INSERT INTO AD_Element (AD_Element_ID,AD_Client_ID,AD_Org_ID,IsActive,Created,CreatedBy,Updated,UpdatedBy,ColumnName,Name,PrintName,EntityType,AD_Element_UU) VALUES (1001000,0,0,'Y',TO_TIMESTAMP('2017-10-14 13:02:28','YYYY-MM-DD HH24:MI:SS'),100,TO_TIMESTAMP('2017-10-14 13:02:28','YYYY-MM-DD HH24:MI:SS'),100,'pdc_status','pdc_status','pdc_status','U','d41e5635-356f-4a11-b0cb-6e3cc3b28338')
;

-- Oct 14, 2017 1:06:38 PM GST
INSERT INTO AD_Reference (AD_Reference_ID,Name,ValidationType,AD_Client_ID,AD_Org_ID,IsActive,Created,CreatedBy,Updated,UpdatedBy,EntityType,IsOrderByValue,AD_Reference_UU) VALUES (1001000,'Pdc_Status','L',0,0,'Y',TO_TIMESTAMP('2017-10-14 13:06:38','YYYY-MM-DD HH24:MI:SS'),100,TO_TIMESTAMP('2017-10-14 13:06:38','YYYY-MM-DD HH24:MI:SS'),100,'U','N','403fe6ad-a7f1-4e6e-81ba-198e82465a32')
;

-- Oct 14, 2017 1:07:20 PM GST
INSERT INTO AD_Ref_List (AD_Ref_List_ID,Name,AD_Reference_ID,Value,AD_Client_ID,AD_Org_ID,IsActive,Created,CreatedBy,Updated,UpdatedBy,EntityType,AD_Ref_List_UU) VALUES (1001000,'PDC Bounced',1001000,'B',0,0,'Y',TO_TIMESTAMP('2017-10-14 13:07:20','YYYY-MM-DD HH24:MI:SS'),100,TO_TIMESTAMP('2017-10-14 13:07:20','YYYY-MM-DD HH24:MI:SS'),100,'U','468cb62d-ea2b-4254-b3a9-a7b66f908d6c')
;

-- Oct 14, 2017 1:07:40 PM GST
INSERT INTO AD_Ref_List (AD_Ref_List_ID,Name,AD_Reference_ID,Value,AD_Client_ID,AD_Org_ID,IsActive,Created,CreatedBy,Updated,UpdatedBy,EntityType,AD_Ref_List_UU) VALUES (1001001,'PDC Withdrawn',1001000,'C',0,0,'Y',TO_TIMESTAMP('2017-10-14 13:07:40','YYYY-MM-DD HH24:MI:SS'),100,TO_TIMESTAMP('2017-10-14 13:07:40','YYYY-MM-DD HH24:MI:SS'),100,'U','2233abbc-5908-49da-ae77-ec031059c713')
;

-- Oct 14, 2017 1:07:44 PM GST
INSERT INTO AD_Ref_List (AD_Ref_List_ID,Name,AD_Reference_ID,Value,AD_Client_ID,AD_Org_ID,IsActive,Created,CreatedBy,Updated,UpdatedBy,EntityType,AD_Ref_List_UU) VALUES (1001002,'PDC Bounced Paid',1001000,'D',0,0,'Y',TO_TIMESTAMP('2017-10-14 13:07:44','YYYY-MM-DD HH24:MI:SS'),100,TO_TIMESTAMP('2017-10-14 13:07:44','YYYY-MM-DD HH24:MI:SS'),100,'U','04293301-a8ab-4675-9fe5-fd3e0c0a89f9')
;

-- Oct 14, 2017 1:07:54 PM GST
INSERT INTO AD_Ref_List (AD_Ref_List_ID,Name,AD_Reference_ID,Value,AD_Client_ID,AD_Org_ID,IsActive,Created,CreatedBy,Updated,UpdatedBy,EntityType,AD_Ref_List_UU) VALUES (1001003,'PDC Withdrawn Paid',1001000,'E',0,0,'Y',TO_TIMESTAMP('2017-10-14 13:07:54','YYYY-MM-DD HH24:MI:SS'),100,TO_TIMESTAMP('2017-10-14 13:07:54','YYYY-MM-DD HH24:MI:SS'),100,'U','b5fe1c09-b15a-4aa5-a308-a51657fb5f0b')
;

-- Oct 14, 2017 1:08:04 PM GST
INSERT INTO AD_Ref_List (AD_Ref_List_ID,Name,AD_Reference_ID,Value,AD_Client_ID,AD_Org_ID,IsActive,Created,CreatedBy,Updated,UpdatedBy,EntityType,AD_Ref_List_UU) VALUES (1001004,'PDC Presented for Collection',1001000,'T',0,0,'Y',TO_TIMESTAMP('2017-10-14 13:08:04','YYYY-MM-DD HH24:MI:SS'),100,TO_TIMESTAMP('2017-10-14 13:08:04','YYYY-MM-DD HH24:MI:SS'),100,'U','bc5426bc-32d3-4ce2-9233-7d49e2e191fb')
;

-- Oct 14, 2017 1:08:13 PM GST
INSERT INTO AD_Ref_List (AD_Ref_List_ID,Name,AD_Reference_ID,Value,AD_Client_ID,AD_Org_ID,IsActive,Created,CreatedBy,Updated,UpdatedBy,EntityType,AD_Ref_List_UU) VALUES (1001005,'PDC Under Collection',1001000,'U',0,0,'Y',TO_TIMESTAMP('2017-10-14 13:08:13','YYYY-MM-DD HH24:MI:SS'),100,TO_TIMESTAMP('2017-10-14 13:08:13','YYYY-MM-DD HH24:MI:SS'),100,'U','af9662e1-9871-4f8f-a8d9-6c40f6c3e618')
;


-- Oct 14, 2017 1:08:31 PM GST
INSERT INTO AD_Column (AD_Column_ID,Version,Name,AD_Table_ID,ColumnName,FieldLength,IsKey,IsParent,IsMandatory,IsTranslated,IsIdentifier,SeqNo,IsEncrypted,AD_Reference_ID,AD_Reference_Value_ID,AD_Client_ID,AD_Org_ID,IsActive,Created,CreatedBy,Updated,UpdatedBy,AD_Element_ID,IsUpdateable,IsSelectionColumn,EntityType,IsSyncDatabase,IsAlwaysUpdateable,IsAutocomplete,IsAllowLogging,AD_Column_UU,IsAllowCopy,SeqNoSelection,IsToolbarButton,IsSecure) VALUES (1001000,0,'pdc_status',335,'pdc_status',1,'N','N','N','N','N',0,'N',17,1001000,0,0,'Y',TO_TIMESTAMP('2017-10-14 13:08:31','YYYY-MM-DD HH24:MI:SS'),100,TO_TIMESTAMP('2017-10-14 13:08:31','YYYY-MM-DD HH24:MI:SS'),100,1001000,'Y','N','U','N','N','N','Y','a5f0eed7-2817-48ac-93a7-e4a7325a62a3','Y',0,'N','N')
;

-- Oct 14, 2017 1:08:38 PM GST
ALTER TABLE C_Payment ADD COLUMN pdc_status CHAR(1) DEFAULT NULL 
;

-- Oct 14, 2017 1:10:13 PM GST
INSERT INTO AD_Element (AD_Element_ID,AD_Client_ID,AD_Org_ID,IsActive,Created,CreatedBy,Updated,UpdatedBy,ColumnName,Name,PrintName,EntityType,AD_Element_UU) VALUES (1001001,0,0,'Y',TO_TIMESTAMP('2017-10-14 13:10:13','YYYY-MM-DD HH24:MI:SS'),100,TO_TIMESTAMP('2017-10-14 13:10:13','YYYY-MM-DD HH24:MI:SS'),100,'pdc_date','pdc_date','pdc_date','U','85f8e908-be17-4e0d-bc56-e506f91328aa')
;

-- Oct 14, 2017 1:10:44 PM GST
INSERT INTO AD_Column (AD_Column_ID,Version,Name,AD_Table_ID,ColumnName,FieldLength,IsKey,IsParent,IsMandatory,IsTranslated,IsIdentifier,SeqNo,IsEncrypted,AD_Reference_ID,AD_Client_ID,AD_Org_ID,IsActive,Created,CreatedBy,Updated,UpdatedBy,AD_Element_ID,IsUpdateable,IsSelectionColumn,EntityType,IsSyncDatabase,IsAlwaysUpdateable,IsAutocomplete,IsAllowLogging,AD_Column_UU,IsAllowCopy,SeqNoSelection,IsToolbarButton,IsSecure) VALUES (1001001,0,'pdc_date',335,'pdc_date',7,'N','N','N','N','N',0,'N',15,0,0,'Y',TO_TIMESTAMP('2017-10-14 13:10:44','YYYY-MM-DD HH24:MI:SS'),100,TO_TIMESTAMP('2017-10-14 13:10:44','YYYY-MM-DD HH24:MI:SS'),100,1001001,'Y','N','U','N','N','N','Y','1b69d3b2-4903-4a6d-8777-07e2f9152403','Y',0,'N','N')
;

-- Oct 14, 2017 1:13:13 PM GST
INSERT INTO AD_Element (AD_Element_ID,AD_Client_ID,AD_Org_ID,IsActive,Created,CreatedBy,Updated,UpdatedBy,ColumnName,Name,PrintName,EntityType,AD_Element_UU) VALUES (1001002,0,0,'Y',TO_TIMESTAMP('2017-10-14 13:13:13','YYYY-MM-DD HH24:MI:SS'),100,TO_TIMESTAMP('2017-10-14 13:13:13','YYYY-MM-DD HH24:MI:SS'),100,'Pdc_Paid_Amt','Pdc_Paid_Amt','Pdc_Paid_Amt','U','8188782d-b133-4253-8bdc-840bc20c20b0')
;

-- Oct 14, 2017 1:14:13 PM GST
INSERT INTO AD_Column (AD_Column_ID,Version,Name,AD_Table_ID,ColumnName,FieldLength,IsKey,IsParent,IsMandatory,IsTranslated,IsIdentifier,SeqNo,IsEncrypted,AD_Reference_ID,AD_Client_ID,AD_Org_ID,IsActive,Created,CreatedBy,Updated,UpdatedBy,AD_Element_ID,IsUpdateable,IsSelectionColumn,EntityType,IsSyncDatabase,IsAlwaysUpdateable,IsAutocomplete,IsAllowLogging,AD_Column_UU,IsAllowCopy,SeqNoSelection,IsToolbarButton,IsSecure) VALUES (1001002,0,'Pdc_Paid_Amt',335,'Pdc_Paid_Amt',131089,'N','N','N','N','N',0,'N',12,0,0,'Y',TO_TIMESTAMP('2017-10-14 13:14:13','YYYY-MM-DD HH24:MI:SS'),100,TO_TIMESTAMP('2017-10-14 13:14:13','YYYY-MM-DD HH24:MI:SS'),100,1001002,'Y','N','U','N','N','N','Y','bdba1f5c-36c6-411e-8b22-58a9ad019f77','Y',0,'N','N')
;

-- Oct 14, 2017 1:14:35 PM GST
ALTER TABLE C_Payment ADD COLUMN pdc_date TIMESTAMP DEFAULT NULL 
;

-- Oct 14, 2017 1:14:59 PM GST
ALTER TABLE C_Payment ADD COLUMN Pdc_Paid_Amt NUMERIC DEFAULT NULL 
;

-- Oct 14, 2017 1:15:27 PM GST
INSERT INTO AD_Element (AD_Element_ID,AD_Client_ID,AD_Org_ID,IsActive,Created,CreatedBy,Updated,UpdatedBy,ColumnName,Name,PrintName,EntityType,AD_Element_UU) VALUES (1001003,0,0,'Y',TO_TIMESTAMP('2017-10-14 13:15:27','YYYY-MM-DD HH24:MI:SS'),100,TO_TIMESTAMP('2017-10-14 13:15:27','YYYY-MM-DD HH24:MI:SS'),100,'Pdc_Amt_Outstanding','Pdc_Amt_Outstanding','Pdc_Amt_Outstanding','U','dad620c0-0471-4aa4-8e69-3f1f9b27da54')
;

-- Oct 14, 2017 1:15:47 PM GST
INSERT INTO AD_Column (AD_Column_ID,Version,Name,AD_Table_ID,ColumnName,FieldLength,IsKey,IsParent,IsMandatory,IsTranslated,IsIdentifier,SeqNo,IsEncrypted,AD_Reference_ID,AD_Client_ID,AD_Org_ID,IsActive,Created,CreatedBy,Updated,UpdatedBy,AD_Element_ID,IsUpdateable,IsSelectionColumn,EntityType,IsSyncDatabase,IsAlwaysUpdateable,IsAutocomplete,IsAllowLogging,AD_Column_UU,IsAllowCopy,SeqNoSelection,IsToolbarButton,IsSecure) VALUES (1001003,0,'Pdc_Amt_Outstanding',335,'Pdc_Amt_Outstanding',131089,'N','N','N','N','N',0,'N',12,0,0,'Y',TO_TIMESTAMP('2017-10-14 13:15:47','YYYY-MM-DD HH24:MI:SS'),100,TO_TIMESTAMP('2017-10-14 13:15:47','YYYY-MM-DD HH24:MI:SS'),100,1001003,'Y','N','U','N','N','N','Y','72690f1e-766b-46b3-b4a0-aab29361e15a','Y',0,'N','N')
;

-- Oct 14, 2017 1:15:51 PM GST
ALTER TABLE C_Payment ADD COLUMN Pdc_Amt_Outstanding NUMERIC DEFAULT NULL 
;

-- Oct 14, 2017 1:16:17 PM GST
INSERT INTO AD_Field (AD_Field_ID,Name,AD_Tab_ID,AD_Column_ID,IsDisplayed,DisplayLength,SeqNo,IsSameLine,IsHeading,IsFieldOnly,IsEncrypted,AD_Client_ID,AD_Org_ID,IsActive,Created,CreatedBy,Updated,UpdatedBy,IsReadOnly,IsCentrallyMaintained,EntityType,AD_Field_UU,IsDisplayedGrid,SeqNoGrid,ColumnSpan) VALUES (1001000,'Deposit Batch',330,208443,'Y',10,760,'N','N','N','N',0,0,'Y',TO_TIMESTAMP('2017-10-14 13:16:17','YYYY-MM-DD HH24:MI:SS'),100,TO_TIMESTAMP('2017-10-14 13:16:17','YYYY-MM-DD HH24:MI:SS'),100,'N','Y','D','9b9febf7-8798-44ec-97e4-de8e2abfc0bf','Y',760,2)
;

-- Oct 14, 2017 1:16:17 PM GST
INSERT INTO AD_Field (AD_Field_ID,Name,AD_Tab_ID,AD_Column_ID,IsDisplayed,DisplayLength,SeqNo,IsSameLine,IsHeading,IsFieldOnly,IsEncrypted,AD_Client_ID,AD_Org_ID,IsActive,Created,CreatedBy,Updated,UpdatedBy,IsReadOnly,IsCentrallyMaintained,EntityType,AD_Field_UU,IsDisplayedGrid,SeqNoGrid,ColumnSpan) VALUES (1001001,'pdc_status',330,1001000,'Y',1,770,'N','N','N','N',0,0,'Y',TO_TIMESTAMP('2017-10-14 13:16:17','YYYY-MM-DD HH24:MI:SS'),100,TO_TIMESTAMP('2017-10-14 13:16:17','YYYY-MM-DD HH24:MI:SS'),100,'N','Y','U','aa120ff8-d32e-4141-bad2-cbe71a8b042e','Y',770,2)
;

-- Oct 14, 2017 1:16:17 PM GST
INSERT INTO AD_Field (AD_Field_ID,Name,AD_Tab_ID,AD_Column_ID,IsDisplayed,DisplayLength,SeqNo,IsSameLine,IsHeading,IsFieldOnly,IsEncrypted,AD_Client_ID,AD_Org_ID,IsActive,Created,CreatedBy,Updated,UpdatedBy,IsReadOnly,IsCentrallyMaintained,EntityType,AD_Field_UU,IsDisplayedGrid,SeqNoGrid,ColumnSpan) VALUES (1001002,'pdc_date',330,1001001,'Y',7,780,'N','N','N','N',0,0,'Y',TO_TIMESTAMP('2017-10-14 13:16:17','YYYY-MM-DD HH24:MI:SS'),100,TO_TIMESTAMP('2017-10-14 13:16:17','YYYY-MM-DD HH24:MI:SS'),100,'N','Y','U','a0de19a0-2761-40ca-aec3-c4ba729926b7','Y',780,2)
;

-- Oct 14, 2017 1:16:17 PM GST
INSERT INTO AD_Field (AD_Field_ID,Name,AD_Tab_ID,AD_Column_ID,IsDisplayed,DisplayLength,SeqNo,IsSameLine,IsHeading,IsFieldOnly,IsEncrypted,AD_Client_ID,AD_Org_ID,IsActive,Created,CreatedBy,Updated,UpdatedBy,IsReadOnly,IsCentrallyMaintained,EntityType,AD_Field_UU,IsDisplayedGrid,SeqNoGrid,ColumnSpan) VALUES (1001003,'Pdc_Paid_Amt',330,1001002,'Y',131089,790,'N','N','N','N',0,0,'Y',TO_TIMESTAMP('2017-10-14 13:16:17','YYYY-MM-DD HH24:MI:SS'),100,TO_TIMESTAMP('2017-10-14 13:16:17','YYYY-MM-DD HH24:MI:SS'),100,'N','Y','U','b99c2e5d-edd4-448f-a24a-cff6edd6cafa','Y',790,5)
;

-- Oct 14, 2017 1:16:17 PM GST
INSERT INTO AD_Field (AD_Field_ID,Name,AD_Tab_ID,AD_Column_ID,IsDisplayed,DisplayLength,SeqNo,IsSameLine,IsHeading,IsFieldOnly,IsEncrypted,AD_Client_ID,AD_Org_ID,IsActive,Created,CreatedBy,Updated,UpdatedBy,IsReadOnly,IsCentrallyMaintained,EntityType,AD_Field_UU,IsDisplayedGrid,SeqNoGrid,ColumnSpan) VALUES (1001004,'Pdc_Amt_Outstanding',330,1001003,'Y',131089,800,'N','N','N','N',0,0,'Y',TO_TIMESTAMP('2017-10-14 13:16:17','YYYY-MM-DD HH24:MI:SS'),100,TO_TIMESTAMP('2017-10-14 13:16:17','YYYY-MM-DD HH24:MI:SS'),100,'N','Y','U','5152fbb3-d2ed-4ebc-ad91-001d6d6a7c0f','Y',800,5)
;

-- Oct 14, 2017 1:17:25 PM GST
UPDATE AD_Field SET SeqNo=360,IsDisplayed='Y' WHERE AD_Field_ID=1001001
;

-- Oct 14, 2017 1:17:25 PM GST
UPDATE AD_Field SET SeqNo=370,IsDisplayed='Y' WHERE AD_Field_ID=1001002
;

-- Oct 14, 2017 1:17:25 PM GST
UPDATE AD_Field SET SeqNo=380,IsDisplayed='Y' WHERE AD_Field_ID=1001003
;

-- Oct 14, 2017 1:17:25 PM GST
UPDATE AD_Field SET SeqNo=390,IsDisplayed='Y' WHERE AD_Field_ID=1001004
;

-- Oct 14, 2017 1:17:25 PM GST
UPDATE AD_Field SET SeqNo=400,IsDisplayed='Y' WHERE AD_Field_ID=4041
;

-- Oct 14, 2017 1:17:25 PM GST
UPDATE AD_Field SET SeqNo=410,IsDisplayed='Y' WHERE AD_Field_ID=4036
;

-- Oct 14, 2017 1:17:25 PM GST
UPDATE AD_Field SET SeqNo=420,IsDisplayed='Y' WHERE AD_Field_ID=4057
;

-- Oct 14, 2017 1:17:25 PM GST
UPDATE AD_Field SET SeqNo=430,IsDisplayed='Y' WHERE AD_Field_ID=4035
;

-- Oct 14, 2017 1:17:25 PM GST
UPDATE AD_Field SET SeqNo=440,IsDisplayed='Y' WHERE AD_Field_ID=4037
;

-- Oct 14, 2017 1:17:25 PM GST
UPDATE AD_Field SET SeqNo=450,IsDisplayed='Y' WHERE AD_Field_ID=4033
;

-- Oct 14, 2017 1:17:25 PM GST
UPDATE AD_Field SET SeqNo=460,IsDisplayed='Y' WHERE AD_Field_ID=4034
;

-- Oct 14, 2017 1:17:25 PM GST
UPDATE AD_Field SET SeqNo=470,IsDisplayed='Y' WHERE AD_Field_ID=4023
;

-- Oct 14, 2017 1:17:25 PM GST
UPDATE AD_Field SET SeqNo=480,IsDisplayed='Y' WHERE AD_Field_ID=4025
;

-- Oct 14, 2017 1:17:25 PM GST
UPDATE AD_Field SET SeqNo=490,IsDisplayed='Y' WHERE AD_Field_ID=4019
;

-- Oct 14, 2017 1:17:25 PM GST
UPDATE AD_Field SET SeqNo=500,IsDisplayed='Y' WHERE AD_Field_ID=4026
;

-- Oct 14, 2017 1:17:25 PM GST
UPDATE AD_Field SET SeqNo=510,IsDisplayed='Y' WHERE AD_Field_ID=4024
;

-- Oct 14, 2017 1:17:25 PM GST
UPDATE AD_Field SET SeqNo=520,IsDisplayed='Y' WHERE AD_Field_ID=6299
;

-- Oct 14, 2017 1:17:25 PM GST
UPDATE AD_Field SET SeqNo=530,IsDisplayed='Y' WHERE AD_Field_ID=4021
;

-- Oct 14, 2017 1:17:25 PM GST
UPDATE AD_Field SET SeqNo=540,IsDisplayed='Y' WHERE AD_Field_ID=4022
;

-- Oct 14, 2017 1:17:25 PM GST
UPDATE AD_Field SET SeqNo=550,IsDisplayed='Y' WHERE AD_Field_ID=4020
;

-- Oct 14, 2017 1:17:25 PM GST
UPDATE AD_Field SET SeqNo=560,IsDisplayed='Y' WHERE AD_Field_ID=4055
;

-- Oct 14, 2017 1:17:25 PM GST
UPDATE AD_Field SET SeqNo=570,IsDisplayed='Y' WHERE AD_Field_ID=4043
;

-- Oct 14, 2017 1:17:25 PM GST
UPDATE AD_Field SET SeqNo=580,IsDisplayed='Y' WHERE AD_Field_ID=4058
;

-- Oct 14, 2017 1:17:25 PM GST
UPDATE AD_Field SET SeqNo=590,IsDisplayed='Y' WHERE AD_Field_ID=4042
;

-- Oct 14, 2017 1:17:25 PM GST
UPDATE AD_Field SET SeqNo=600,IsDisplayed='Y' WHERE AD_Field_ID=4258
;

-- Oct 14, 2017 1:17:25 PM GST
UPDATE AD_Field SET SeqNo=610,IsDisplayed='Y' WHERE AD_Field_ID=4039
;

-- Oct 14, 2017 1:17:25 PM GST
UPDATE AD_Field SET SeqNo=620,IsDisplayed='Y' WHERE AD_Field_ID=4053
;

-- Oct 14, 2017 1:17:25 PM GST
UPDATE AD_Field SET SeqNo=630,IsDisplayed='Y' WHERE AD_Field_ID=4052
;

-- Oct 14, 2017 1:17:25 PM GST
UPDATE AD_Field SET SeqNo=640,IsDisplayed='Y' WHERE AD_Field_ID=200631
;

-- Oct 14, 2017 1:17:25 PM GST
UPDATE AD_Field SET SeqNo=650,IsDisplayed='Y' WHERE AD_Field_ID=200630
;

-- Oct 14, 2017 1:17:25 PM GST
UPDATE AD_Field SET SeqNo=660,IsDisplayed='Y' WHERE AD_Field_ID=4051
;

-- Oct 14, 2017 1:17:25 PM GST
UPDATE AD_Field SET SeqNo=670,IsDisplayed='Y' WHERE AD_Field_ID=4047
;

-- Oct 14, 2017 1:17:25 PM GST
UPDATE AD_Field SET SeqNo=680,IsDisplayed='Y' WHERE AD_Field_ID=4049
;

-- Oct 14, 2017 1:17:25 PM GST
UPDATE AD_Field SET SeqNo=690,IsDisplayed='Y' WHERE AD_Field_ID=4048
;

-- Oct 14, 2017 1:17:25 PM GST
UPDATE AD_Field SET SeqNo=700,IsDisplayed='Y' WHERE AD_Field_ID=200627
;

-- Oct 14, 2017 1:17:25 PM GST
UPDATE AD_Field SET SeqNo=710,IsDisplayed='Y' WHERE AD_Field_ID=200625
;

-- Oct 14, 2017 1:17:25 PM GST
UPDATE AD_Field SET SeqNo=720,IsDisplayed='Y' WHERE AD_Field_ID=200626
;

-- Oct 14, 2017 1:17:25 PM GST
UPDATE AD_Field SET SeqNo=730,IsDisplayed='Y' WHERE AD_Field_ID=200624
;

-- Oct 14, 2017 1:17:25 PM GST
UPDATE AD_Field SET SeqNo=740,IsDisplayed='Y' WHERE AD_Field_ID=4362
;

-- Oct 14, 2017 1:17:25 PM GST
UPDATE AD_Field SET SeqNo=750,IsDisplayed='Y' WHERE AD_Field_ID=4361
;

-- Oct 14, 2017 1:17:25 PM GST
UPDATE AD_Field SET SeqNo=760,IsDisplayed='Y' WHERE AD_Field_ID=6552
;

-- Oct 14, 2017 1:17:25 PM GST
UPDATE AD_Field SET SeqNo=770,IsDisplayed='Y' WHERE AD_Field_ID=4044
;

-- Oct 14, 2017 1:17:25 PM GST
UPDATE AD_Field SET SeqNo=780,IsDisplayed='Y' WHERE AD_Field_ID=4266
;

-- Oct 14, 2017 1:17:25 PM GST
UPDATE AD_Field SET SeqNo=790,IsDisplayed='Y' WHERE AD_Field_ID=4040
;

-- Oct 14, 2017 1:17:25 PM GST
UPDATE AD_Field SET SeqNo=800,IsDisplayed='Y' WHERE AD_Field_ID=1001000
;

-- Oct 14, 2017 1:19:14 PM GST
UPDATE AD_Field SET IsDisplayed='Y', SeqNo=360, AD_Reference_Value_ID=NULL, AD_Val_Rule_ID=NULL, XPosition=1, IsToolbarButton=NULL,Updated=TO_TIMESTAMP('2017-10-14 13:19:14','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=1001002
;

-- Oct 14, 2017 1:19:14 PM GST
UPDATE AD_Field SET SeqNo=370, AD_Reference_Value_ID=NULL, AD_Val_Rule_ID=NULL, IsToolbarButton=NULL,Updated=TO_TIMESTAMP('2017-10-14 13:19:14','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=1001001
;

-- Oct 14, 2017 1:19:14 PM GST
UPDATE AD_Field SET SeqNo=380, AD_Reference_Value_ID=NULL, AD_Val_Rule_ID=NULL, ColumnSpan=2, IsToolbarButton=NULL,Updated=TO_TIMESTAMP('2017-10-14 13:19:14','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=1001003
;

-- Oct 14, 2017 1:19:14 PM GST
UPDATE AD_Field SET IsDisplayed='Y', SeqNo=390, AD_Reference_Value_ID=NULL, AD_Val_Rule_ID=NULL, XPosition=4, ColumnSpan=2, IsToolbarButton=NULL,Updated=TO_TIMESTAMP('2017-10-14 13:19:14','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=1001004
;

-- Oct 14, 2017 1:22:06 PM GST
UPDATE AD_Field SET DisplayLogic='@TenderType@=K', IsReadOnly='Y', DefaultValue='@SQL=SELECT coalesce(pdc_status,''U'') AS DefaultValue FROM AD_OrgInfo where AD_Org_ID = @#AD_Org_ID@', AD_Reference_Value_ID=NULL, AD_Val_Rule_ID=NULL, MandatoryLogic='@TenderType@=K', IsToolbarButton=NULL,Updated=TO_TIMESTAMP('2017-10-14 13:22:06','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=1001001
;

-- Oct 14, 2017 1:22:52 PM GST
UPDATE AD_Field SET DisplayLogic='@Ref_Payment_ID@>0', IsReadOnly='Y', AD_Reference_Value_ID=NULL, AD_Val_Rule_ID=NULL, IsToolbarButton=NULL,Updated=TO_TIMESTAMP('2017-10-14 13:22:52','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=1001004
;

-- Oct 14, 2017 1:28:40 PM GST
UPDATE AD_Field SET SeqNo=160,IsDisplayed='Y' WHERE AD_Field_ID=52053
;

-- Oct 14, 2017 1:28:40 PM GST
UPDATE AD_Field SET SeqNo=170,IsDisplayed='Y' WHERE AD_Field_ID=1001004
;

-- Oct 14, 2017 1:28:40 PM GST
UPDATE AD_Field SET SeqNo=180,IsDisplayed='Y' WHERE AD_Field_ID=7809
;

-- Oct 14, 2017 1:28:40 PM GST
UPDATE AD_Field SET SeqNo=190,IsDisplayed='Y' WHERE AD_Field_ID=7807
;

-- Oct 14, 2017 1:28:40 PM GST
UPDATE AD_Field SET SeqNo=200,IsDisplayed='Y' WHERE AD_Field_ID=7808
;

-- Oct 14, 2017 1:28:40 PM GST
UPDATE AD_Field SET SeqNo=210,IsDisplayed='Y' WHERE AD_Field_ID=7806
;

-- Oct 14, 2017 1:28:40 PM GST
UPDATE AD_Field SET SeqNo=220,IsDisplayed='Y' WHERE AD_Field_ID=7810
;

-- Oct 14, 2017 1:28:40 PM GST
UPDATE AD_Field SET SeqNo=230,IsDisplayed='Y' WHERE AD_Field_ID=4133
;

-- Oct 14, 2017 1:28:40 PM GST
UPDATE AD_Field SET SeqNo=240,IsDisplayed='Y' WHERE AD_Field_ID=4129
;

-- Oct 14, 2017 1:28:40 PM GST
UPDATE AD_Field SET SeqNo=250,IsDisplayed='Y' WHERE AD_Field_ID=8651
;

-- Oct 14, 2017 1:28:40 PM GST
UPDATE AD_Field SET SeqNo=260,IsDisplayed='Y' WHERE AD_Field_ID=4131
;

-- Oct 14, 2017 1:28:40 PM GST
UPDATE AD_Field SET SeqNo=270,IsDisplayed='Y' WHERE AD_Field_ID=5117
;

-- Oct 14, 2017 1:28:40 PM GST
UPDATE AD_Field SET SeqNo=280,IsDisplayed='Y' WHERE AD_Field_ID=5736
;

-- Oct 14, 2017 1:28:40 PM GST
UPDATE AD_Field SET SeqNo=290,IsDisplayed='Y' WHERE AD_Field_ID=5737
;

-- Oct 14, 2017 1:28:40 PM GST
UPDATE AD_Field SET SeqNo=300,IsDisplayed='Y' WHERE AD_Field_ID=4056
;

-- Oct 14, 2017 1:28:40 PM GST
UPDATE AD_Field SET SeqNo=310,IsDisplayed='Y' WHERE AD_Field_ID=200472
;

-- Oct 14, 2017 1:28:40 PM GST
UPDATE AD_Field SET SeqNo=320,IsDisplayed='Y' WHERE AD_Field_ID=4363
;

-- Oct 14, 2017 1:28:40 PM GST
UPDATE AD_Field SET SeqNo=330,IsDisplayed='Y' WHERE AD_Field_ID=4054
;

-- Oct 14, 2017 1:28:40 PM GST
UPDATE AD_Field SET SeqNo=340,IsDisplayed='Y' WHERE AD_Field_ID=4027
;

-- Oct 14, 2017 1:28:40 PM GST
UPDATE AD_Field SET SeqNo=350,IsDisplayed='Y' WHERE AD_Field_ID=204358
;

-- Oct 14, 2017 1:28:40 PM GST
UPDATE AD_Field SET SeqNo=360,IsDisplayed='Y' WHERE AD_Field_ID=204357
;

-- Oct 14, 2017 1:28:40 PM GST
UPDATE AD_Field SET SeqNo=370,IsDisplayed='Y' WHERE AD_Field_ID=4032
;

-- Oct 14, 2017 1:28:40 PM GST
UPDATE AD_Field SET SeqNo=380,IsDisplayed='Y' WHERE AD_Field_ID=1001002
;

-- Oct 14, 2017 1:28:40 PM GST
UPDATE AD_Field SET SeqNo=390,IsDisplayed='Y' WHERE AD_Field_ID=1001001
;

-- Oct 14, 2017 1:28:40 PM GST
UPDATE AD_Field SET SeqNo=400,IsDisplayed='Y' WHERE AD_Field_ID=1001003
;

-- Oct 14, 2017 1:28:40 PM GST
UPDATE AD_Field SET SeqNo=410,IsDisplayed='Y' WHERE AD_Field_ID=4041
;

-- Oct 14, 2017 1:28:40 PM GST
UPDATE AD_Field SET SeqNo=420,IsDisplayed='Y' WHERE AD_Field_ID=4036
;

-- Oct 14, 2017 1:28:40 PM GST
UPDATE AD_Field SET SeqNo=430,IsDisplayed='Y' WHERE AD_Field_ID=4057
;

-- Oct 14, 2017 1:28:40 PM GST
UPDATE AD_Field SET SeqNo=440,IsDisplayed='Y' WHERE AD_Field_ID=4035
;

-- Oct 14, 2017 1:28:40 PM GST
UPDATE AD_Field SET SeqNo=450,IsDisplayed='Y' WHERE AD_Field_ID=4037
;

-- Oct 14, 2017 1:28:40 PM GST
UPDATE AD_Field SET SeqNo=460,IsDisplayed='Y' WHERE AD_Field_ID=4033
;

-- Oct 14, 2017 1:28:40 PM GST
UPDATE AD_Field SET SeqNo=470,IsDisplayed='Y' WHERE AD_Field_ID=4034
;

-- Oct 14, 2017 1:28:40 PM GST
UPDATE AD_Field SET SeqNo=480,IsDisplayed='Y' WHERE AD_Field_ID=4023
;

-- Oct 14, 2017 1:28:40 PM GST
UPDATE AD_Field SET SeqNo=490,IsDisplayed='Y' WHERE AD_Field_ID=4025
;

-- Oct 14, 2017 1:28:40 PM GST
UPDATE AD_Field SET SeqNo=500,IsDisplayed='Y' WHERE AD_Field_ID=4019
;

-- Oct 14, 2017 1:28:40 PM GST
UPDATE AD_Field SET SeqNo=510,IsDisplayed='Y' WHERE AD_Field_ID=4026
;

-- Oct 14, 2017 1:28:40 PM GST
UPDATE AD_Field SET SeqNo=520,IsDisplayed='Y' WHERE AD_Field_ID=4024
;

-- Oct 14, 2017 1:28:40 PM GST
UPDATE AD_Field SET SeqNo=530,IsDisplayed='Y' WHERE AD_Field_ID=6299
;

-- Oct 14, 2017 1:28:40 PM GST
UPDATE AD_Field SET SeqNo=540,IsDisplayed='Y' WHERE AD_Field_ID=4021
;

-- Oct 14, 2017 1:28:40 PM GST
UPDATE AD_Field SET SeqNo=550,IsDisplayed='Y' WHERE AD_Field_ID=4022
;

-- Oct 14, 2017 1:28:40 PM GST
UPDATE AD_Field SET SeqNo=560,IsDisplayed='Y' WHERE AD_Field_ID=4020
;

-- Oct 14, 2017 1:28:40 PM GST
UPDATE AD_Field SET SeqNo=570,IsDisplayed='Y' WHERE AD_Field_ID=4055
;

-- Oct 14, 2017 1:28:40 PM GST
UPDATE AD_Field SET SeqNo=580,IsDisplayed='Y' WHERE AD_Field_ID=4043
;

-- Oct 14, 2017 1:28:40 PM GST
UPDATE AD_Field SET SeqNo=590,IsDisplayed='Y' WHERE AD_Field_ID=4058
;

-- Oct 14, 2017 1:28:40 PM GST
UPDATE AD_Field SET SeqNo=600,IsDisplayed='Y' WHERE AD_Field_ID=4042
;

-- Oct 14, 2017 1:28:40 PM GST
UPDATE AD_Field SET SeqNo=610,IsDisplayed='Y' WHERE AD_Field_ID=4258
;

-- Oct 14, 2017 1:28:40 PM GST
UPDATE AD_Field SET SeqNo=620,IsDisplayed='Y' WHERE AD_Field_ID=4039
;

-- Oct 14, 2017 1:28:40 PM GST
UPDATE AD_Field SET SeqNo=630,IsDisplayed='Y' WHERE AD_Field_ID=4053
;

-- Oct 14, 2017 1:28:40 PM GST
UPDATE AD_Field SET SeqNo=640,IsDisplayed='Y' WHERE AD_Field_ID=4052
;

-- Oct 14, 2017 1:28:40 PM GST
UPDATE AD_Field SET SeqNo=650,IsDisplayed='Y' WHERE AD_Field_ID=200631
;

-- Oct 14, 2017 1:28:40 PM GST
UPDATE AD_Field SET SeqNo=660,IsDisplayed='Y' WHERE AD_Field_ID=200630
;

-- Oct 14, 2017 1:28:40 PM GST
UPDATE AD_Field SET SeqNo=670,IsDisplayed='Y' WHERE AD_Field_ID=4051
;

-- Oct 14, 2017 1:28:40 PM GST
UPDATE AD_Field SET SeqNo=680,IsDisplayed='Y' WHERE AD_Field_ID=4047
;

-- Oct 14, 2017 1:28:40 PM GST
UPDATE AD_Field SET SeqNo=690,IsDisplayed='Y' WHERE AD_Field_ID=4049
;

-- Oct 14, 2017 1:28:41 PM GST
UPDATE AD_Field SET SeqNo=700,IsDisplayed='Y' WHERE AD_Field_ID=4048
;

-- Oct 14, 2017 1:28:41 PM GST
UPDATE AD_Field SET SeqNo=710,IsDisplayed='Y' WHERE AD_Field_ID=200627
;

-- Oct 14, 2017 1:28:41 PM GST
UPDATE AD_Field SET SeqNo=720,IsDisplayed='Y' WHERE AD_Field_ID=200625
;

-- Oct 14, 2017 1:28:41 PM GST
UPDATE AD_Field SET SeqNo=730,IsDisplayed='Y' WHERE AD_Field_ID=200626
;

-- Oct 14, 2017 1:28:41 PM GST
UPDATE AD_Field SET SeqNo=740,IsDisplayed='Y' WHERE AD_Field_ID=200624
;

-- Oct 14, 2017 1:28:41 PM GST
UPDATE AD_Field SET SeqNo=750,IsDisplayed='Y' WHERE AD_Field_ID=4362
;

-- Oct 14, 2017 1:28:41 PM GST
UPDATE AD_Field SET SeqNo=760,IsDisplayed='Y' WHERE AD_Field_ID=4361
;

-- Oct 14, 2017 1:28:41 PM GST
UPDATE AD_Field SET SeqNo=770,IsDisplayed='Y' WHERE AD_Field_ID=6552
;

-- Oct 14, 2017 1:28:41 PM GST
UPDATE AD_Field SET SeqNo=780,IsDisplayed='Y' WHERE AD_Field_ID=4044
;

-- Oct 14, 2017 1:28:41 PM GST
UPDATE AD_Field SET SeqNo=790,IsDisplayed='Y' WHERE AD_Field_ID=4266
;

-- Oct 14, 2017 1:28:41 PM GST
UPDATE AD_Field SET SeqNo=800,IsDisplayed='Y' WHERE AD_Field_ID=4040
;

-- Oct 14, 2017 1:28:41 PM GST
UPDATE AD_Field SET SeqNo=810,IsDisplayed='Y' WHERE AD_Field_ID=1001000
;

-- Oct 14, 2017 1:29:39 PM GST
INSERT INTO AD_Val_Rule (AD_Val_Rule_ID,Name,Type,Code,AD_Client_ID,AD_Org_ID,IsActive,Created,CreatedBy,Updated,UpdatedBy,EntityType,AD_Val_Rule_UU) VALUES (1001000,'C_Payment Withdrawn / Bounced (bPartner)','S','C_Payment_ID in (SELECT C_Payment_ID FROM C_Payment cp where cp.C_BPartner_ID = @C_BPartner_ID@ AND cp.DocStatus =''CO'' AND cp.PayAmt>0 AND cp.pdc_status=''B'' AND coalesce(cp.pdc_paid_amt,0) < cp.payamt)',0,0,'Y',TO_TIMESTAMP('2017-10-14 13:29:39','YYYY-MM-DD HH24:MI:SS'),100,TO_TIMESTAMP('2017-10-14 13:29:39','YYYY-MM-DD HH24:MI:SS'),100,'U','11bd9163-84a3-4ba1-a97f-013bfd113066')
;

-- Oct 14, 2017 1:29:47 PM GST
UPDATE AD_Field SET AD_Reference_ID=18, AD_Val_Rule_ID=1001000,Updated=TO_TIMESTAMP('2017-10-14 13:29:47','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=52053
;

-- Oct 14, 2017 1:30:13 PM GST
UPDATE AD_Field SET IsUpdateable='Y',Updated=TO_TIMESTAMP('2017-10-14 13:30:13','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=52053
;

-- Oct 14, 2017 1:31:34 PM GST
UPDATE AD_Field SET DisplayLogic='@TenderType@=K', AD_Reference_Value_ID=NULL, AD_Val_Rule_ID=NULL, IsToolbarButton=NULL,Updated=TO_TIMESTAMP('2017-10-14 13:31:34','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=1001002
;

-- Oct 14, 2017 1:33:45 PM GST
INSERT INTO AD_Column (AD_Column_ID,Version,Name,AD_Table_ID,ColumnName,FieldLength,IsKey,IsParent,IsMandatory,IsTranslated,IsIdentifier,SeqNo,IsEncrypted,AD_Reference_ID,AD_Reference_Value_ID,AD_Client_ID,AD_Org_ID,IsActive,Created,CreatedBy,Updated,UpdatedBy,AD_Element_ID,IsUpdateable,IsSelectionColumn,EntityType,IsSyncDatabase,IsAlwaysUpdateable,IsAutocomplete,IsAllowLogging,AD_Column_UU,IsAllowCopy,SeqNoSelection,IsToolbarButton,IsSecure,FKConstraintType) VALUES (1001004,0,'pdc_status',228,'pdc_status',1,'N','N','N','N','N',0,'N',17,1001000,0,0,'Y',TO_TIMESTAMP('2017-10-14 13:33:45','YYYY-MM-DD HH24:MI:SS'),100,TO_TIMESTAMP('2017-10-14 13:33:45','YYYY-MM-DD HH24:MI:SS'),100,1001000,'Y','N','U','N','N','N','Y','4ab53e59-485f-48d4-97ac-32f55526e0d8','Y',0,'N','N','N')
;

-- Oct 14, 2017 1:34:23 PM GST
INSERT INTO AD_Field (AD_Field_ID,Name,AD_Tab_ID,AD_Column_ID,IsDisplayed,DisplayLength,IsSameLine,IsHeading,IsFieldOnly,IsEncrypted,AD_Client_ID,AD_Org_ID,IsActive,Created,CreatedBy,Updated,UpdatedBy,IsReadOnly,IsCentrallyMaintained,EntityType,AD_Field_UU,IsDisplayedGrid,ColumnSpan) VALUES (1001005,'AD_OrgInfo_UU',170,60441,'N',36,'N','N','N','N',0,0,'Y',TO_TIMESTAMP('2017-10-14 13:34:23','YYYY-MM-DD HH24:MI:SS'),100,TO_TIMESTAMP('2017-10-14 13:34:23','YYYY-MM-DD HH24:MI:SS'),100,'N','Y','D','36fdc5e7-d5d2-406f-8bec-38d90c0196d3','N',2)
;

-- Oct 14, 2017 1:34:23 PM GST
INSERT INTO AD_Field (AD_Field_ID,Name,AD_Tab_ID,AD_Column_ID,IsDisplayed,DisplayLength,SeqNo,IsSameLine,IsHeading,IsFieldOnly,IsEncrypted,AD_Client_ID,AD_Org_ID,IsActive,Created,CreatedBy,Updated,UpdatedBy,IsReadOnly,IsCentrallyMaintained,EntityType,AD_Field_UU,IsDisplayedGrid,SeqNoGrid,ColumnSpan) VALUES (1001006,'pdc_status',170,1001004,'Y',1,180,'N','N','N','N',0,0,'Y',TO_TIMESTAMP('2017-10-14 13:34:23','YYYY-MM-DD HH24:MI:SS'),100,TO_TIMESTAMP('2017-10-14 13:34:23','YYYY-MM-DD HH24:MI:SS'),100,'N','Y','U','5432b757-885c-442c-a582-738c50f227a9','Y',180,2)
;

-- Oct 14, 2017 1:35:02 PM GST
ALTER TABLE AD_OrgInfo ADD COLUMN pdc_status CHAR(1) DEFAULT NULL 
;

-- Oct 14, 2017 1:36:38 PM GST
UPDATE AD_Field SET Name='PDC Status', AD_Reference_Value_ID=NULL, AD_Val_Rule_ID=NULL, IsToolbarButton=NULL,Updated=TO_TIMESTAMP('2017-10-14 13:36:38','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=1001006
;

-- Oct 14, 2017 1:36:56 PM GST
UPDATE AD_Field SET IsDisplayed='Y', SeqNo=130, AD_Reference_Value_ID=NULL, AD_Val_Rule_ID=NULL, XPosition=1, IsToolbarButton=NULL,Updated=TO_TIMESTAMP('2017-10-14 13:36:56','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=1001006
;

-- Oct 14, 2017 1:36:56 PM GST
UPDATE AD_Field SET SeqNo=140, AD_Reference_Value_ID=NULL, AD_Val_Rule_ID=NULL, IsToolbarButton=NULL,Updated=TO_TIMESTAMP('2017-10-14 13:36:56','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=58855
;

-- Oct 14, 2017 1:36:56 PM GST
UPDATE AD_Field SET SeqNo=150, AD_Reference_Value_ID=NULL, AD_Val_Rule_ID=NULL, IsToolbarButton=NULL,Updated=TO_TIMESTAMP('2017-10-14 13:36:56','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=58852
;

-- Oct 14, 2017 1:36:56 PM GST
UPDATE AD_Field SET SeqNo=160, AD_Reference_Value_ID=NULL, AD_Val_Rule_ID=NULL, IsToolbarButton=NULL,Updated=TO_TIMESTAMP('2017-10-14 13:36:56','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=58854
;

-- Oct 14, 2017 1:36:56 PM GST
UPDATE AD_Field SET SeqNo=170, AD_Reference_Value_ID=NULL, AD_Val_Rule_ID=NULL, IsToolbarButton=NULL,Updated=TO_TIMESTAMP('2017-10-14 13:36:56','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=58853
;

-- Oct 14, 2017 1:36:56 PM GST
UPDATE AD_Field SET SeqNo=180, AD_Reference_Value_ID=NULL, AD_Val_Rule_ID=NULL, IsToolbarButton=NULL,Updated=TO_TIMESTAMP('2017-10-14 13:36:56','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=57532
;

-- Oct 14, 2017 1:36:56 PM GST
UPDATE AD_Field SET SeqNo=0, AD_Reference_Value_ID=NULL, AD_Val_Rule_ID=NULL, IsToolbarButton=NULL,Updated=TO_TIMESTAMP('2017-10-14 13:36:56','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=1001005
;

-- Oct 14, 2017 1:37:53 PM GST
DELETE FROM AD_Field_Trl WHERE AD_Field_ID=1001003
;

-- Oct 14, 2017 1:37:53 PM GST
DELETE FROM AD_Field WHERE AD_Field_ID=1001003
;

-- Oct 14, 2017 1:38:25 PM GST
UPDATE AD_Field SET IsDisplayed='Y', SeqNo=380, AD_Reference_Value_ID=NULL, AD_Val_Rule_ID=NULL, XPosition=4, IsToolbarButton=NULL,Updated=TO_TIMESTAMP('2017-10-14 13:38:25','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=1001002
;

-- Oct 14, 2017 1:38:25 PM GST
UPDATE AD_Field SET SeqNo=400, AD_Reference_Value_ID=NULL, AD_Val_Rule_ID=NULL, IsToolbarButton=NULL,Updated=TO_TIMESTAMP('2017-10-14 13:38:25','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=4041
;

-- Oct 14, 2017 1:38:25 PM GST
UPDATE AD_Field SET SeqNo=410, AD_Reference_Value_ID=NULL, AD_Val_Rule_ID=NULL, IsToolbarButton=NULL,Updated=TO_TIMESTAMP('2017-10-14 13:38:25','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=4036
;

-- Oct 14, 2017 1:38:25 PM GST
UPDATE AD_Field SET SeqNo=420, AD_Reference_Value_ID=NULL, AD_Val_Rule_ID=NULL, IsToolbarButton=NULL,Updated=TO_TIMESTAMP('2017-10-14 13:38:25','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=4057
;

-- Oct 14, 2017 1:38:25 PM GST
UPDATE AD_Field SET SeqNo=430, AD_Reference_Value_ID=NULL, AD_Val_Rule_ID=NULL, IsToolbarButton=NULL,Updated=TO_TIMESTAMP('2017-10-14 13:38:25','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=4035
;

-- Oct 14, 2017 1:38:25 PM GST
UPDATE AD_Field SET SeqNo=440, AD_Reference_Value_ID=NULL, AD_Val_Rule_ID=NULL, IsToolbarButton=NULL,Updated=TO_TIMESTAMP('2017-10-14 13:38:25','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=4037
;

-- Oct 14, 2017 1:38:25 PM GST
UPDATE AD_Field SET SeqNo=450, AD_Reference_Value_ID=NULL, AD_Val_Rule_ID=NULL, IsToolbarButton=NULL,Updated=TO_TIMESTAMP('2017-10-14 13:38:25','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=4033
;

-- Oct 14, 2017 1:38:25 PM GST
UPDATE AD_Field SET SeqNo=460, AD_Reference_Value_ID=NULL, AD_Val_Rule_ID=NULL, IsToolbarButton=NULL,Updated=TO_TIMESTAMP('2017-10-14 13:38:25','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=4034
;

-- Oct 14, 2017 1:38:25 PM GST
UPDATE AD_Field SET SeqNo=470, AD_Reference_Value_ID=NULL, AD_Val_Rule_ID=NULL, IsToolbarButton=NULL,Updated=TO_TIMESTAMP('2017-10-14 13:38:25','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=4023
;

-- Oct 14, 2017 1:38:25 PM GST
UPDATE AD_Field SET SeqNo=480, AD_Reference_Value_ID=NULL, AD_Val_Rule_ID=NULL, IsToolbarButton=NULL,Updated=TO_TIMESTAMP('2017-10-14 13:38:25','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=4025
;

-- Oct 14, 2017 1:38:25 PM GST
UPDATE AD_Field SET SeqNo=490, AD_Reference_Value_ID=NULL, AD_Val_Rule_ID=NULL, IsToolbarButton=NULL,Updated=TO_TIMESTAMP('2017-10-14 13:38:25','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=4019
;

-- Oct 14, 2017 1:38:25 PM GST
UPDATE AD_Field SET SeqNo=500, AD_Reference_Value_ID=NULL, AD_Val_Rule_ID=NULL, IsToolbarButton=NULL,Updated=TO_TIMESTAMP('2017-10-14 13:38:25','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=4026
;

-- Oct 14, 2017 1:38:25 PM GST
UPDATE AD_Field SET SeqNo=510, AD_Reference_Value_ID=NULL, AD_Val_Rule_ID=NULL, IsToolbarButton=NULL,Updated=TO_TIMESTAMP('2017-10-14 13:38:25','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=4024
;

-- Oct 14, 2017 1:38:25 PM GST
UPDATE AD_Field SET SeqNo=520, AD_Reference_Value_ID=NULL, AD_Val_Rule_ID=NULL, IsToolbarButton=NULL,Updated=TO_TIMESTAMP('2017-10-14 13:38:25','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=6299
;

-- Oct 14, 2017 1:38:25 PM GST
UPDATE AD_Field SET SeqNo=530, AD_Reference_Value_ID=NULL, AD_Val_Rule_ID=NULL, IsToolbarButton=NULL,Updated=TO_TIMESTAMP('2017-10-14 13:38:25','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=4021
;

-- Oct 14, 2017 1:38:25 PM GST
UPDATE AD_Field SET SeqNo=540, AD_Reference_Value_ID=NULL, AD_Val_Rule_ID=NULL, IsToolbarButton=NULL,Updated=TO_TIMESTAMP('2017-10-14 13:38:25','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=4022
;

-- Oct 14, 2017 1:38:25 PM GST
UPDATE AD_Field SET SeqNo=550, AD_Reference_Value_ID=NULL, AD_Val_Rule_ID=NULL, IsToolbarButton=NULL,Updated=TO_TIMESTAMP('2017-10-14 13:38:25','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=4020
;

-- Oct 14, 2017 1:38:25 PM GST
UPDATE AD_Field SET SeqNo=560, AD_Reference_Value_ID=NULL, AD_Val_Rule_ID=NULL, IsToolbarButton=NULL,Updated=TO_TIMESTAMP('2017-10-14 13:38:25','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=4055
;

-- Oct 14, 2017 1:38:25 PM GST
UPDATE AD_Field SET SeqNo=570, AD_Reference_Value_ID=NULL, AD_Val_Rule_ID=NULL, IsToolbarButton=NULL,Updated=TO_TIMESTAMP('2017-10-14 13:38:25','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=4043
;

-- Oct 14, 2017 1:38:25 PM GST
UPDATE AD_Field SET SeqNo=580, AD_Reference_Value_ID=NULL, AD_Val_Rule_ID=NULL, IsToolbarButton=NULL,Updated=TO_TIMESTAMP('2017-10-14 13:38:25','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=4058
;

-- Oct 14, 2017 1:38:26 PM GST
UPDATE AD_Field SET SeqNo=590, AD_Reference_Value_ID=NULL, AD_Val_Rule_ID=NULL, IsToolbarButton=NULL,Updated=TO_TIMESTAMP('2017-10-14 13:38:26','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=4042
;

-- Oct 14, 2017 1:38:26 PM GST
UPDATE AD_Field SET SeqNo=600, AD_Reference_Value_ID=NULL, AD_Val_Rule_ID=NULL, IsToolbarButton=NULL,Updated=TO_TIMESTAMP('2017-10-14 13:38:26','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=4258
;

-- Oct 14, 2017 1:38:26 PM GST
UPDATE AD_Field SET SeqNo=610, AD_Reference_Value_ID=NULL, AD_Val_Rule_ID=NULL, IsToolbarButton=NULL,Updated=TO_TIMESTAMP('2017-10-14 13:38:26','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=4039
;

-- Oct 14, 2017 1:38:26 PM GST
UPDATE AD_Field SET SeqNo=620, AD_Reference_Value_ID=NULL, AD_Val_Rule_ID=NULL, IsToolbarButton=NULL,Updated=TO_TIMESTAMP('2017-10-14 13:38:26','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=4053
;

-- Oct 14, 2017 1:38:26 PM GST
UPDATE AD_Field SET SeqNo=630, AD_Reference_Value_ID=NULL, AD_Val_Rule_ID=NULL, IsToolbarButton=NULL,Updated=TO_TIMESTAMP('2017-10-14 13:38:26','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=4052
;

-- Oct 14, 2017 1:38:26 PM GST
UPDATE AD_Field SET SeqNo=640, AD_Reference_Value_ID=NULL, AD_Val_Rule_ID=NULL, IsToolbarButton=NULL,Updated=TO_TIMESTAMP('2017-10-14 13:38:26','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=200631
;

-- Oct 14, 2017 1:38:26 PM GST
UPDATE AD_Field SET SeqNo=650, AD_Reference_Value_ID=NULL, AD_Val_Rule_ID=NULL, IsToolbarButton=NULL,Updated=TO_TIMESTAMP('2017-10-14 13:38:26','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=200630
;

-- Oct 14, 2017 1:38:26 PM GST
UPDATE AD_Field SET SeqNo=660, AD_Reference_Value_ID=NULL, AD_Val_Rule_ID=NULL, IsToolbarButton=NULL,Updated=TO_TIMESTAMP('2017-10-14 13:38:26','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=4051
;

-- Oct 14, 2017 1:38:26 PM GST
UPDATE AD_Field SET SeqNo=670, AD_Reference_Value_ID=NULL, AD_Val_Rule_ID=NULL, IsToolbarButton=NULL,Updated=TO_TIMESTAMP('2017-10-14 13:38:26','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=4047
;

-- Oct 14, 2017 1:38:26 PM GST
UPDATE AD_Field SET SeqNo=680, AD_Reference_Value_ID=NULL, AD_Val_Rule_ID=NULL, IsToolbarButton=NULL,Updated=TO_TIMESTAMP('2017-10-14 13:38:26','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=4049
;

-- Oct 14, 2017 1:38:26 PM GST
UPDATE AD_Field SET SeqNo=690, AD_Reference_Value_ID=NULL, AD_Val_Rule_ID=NULL, IsToolbarButton=NULL,Updated=TO_TIMESTAMP('2017-10-14 13:38:26','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=4048
;

-- Oct 14, 2017 1:38:26 PM GST
UPDATE AD_Field SET SeqNo=700, AD_Reference_Value_ID=NULL, AD_Val_Rule_ID=NULL, IsToolbarButton=NULL,Updated=TO_TIMESTAMP('2017-10-14 13:38:26','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=200627
;

-- Oct 14, 2017 1:38:26 PM GST
UPDATE AD_Field SET SeqNo=710, AD_Reference_Value_ID=NULL, AD_Val_Rule_ID=NULL, IsToolbarButton=NULL,Updated=TO_TIMESTAMP('2017-10-14 13:38:26','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=200625
;

-- Oct 14, 2017 1:38:26 PM GST
UPDATE AD_Field SET SeqNo=720, AD_Reference_Value_ID=NULL, AD_Val_Rule_ID=NULL, IsToolbarButton=NULL,Updated=TO_TIMESTAMP('2017-10-14 13:38:26','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=200626
;

-- Oct 14, 2017 1:38:26 PM GST
UPDATE AD_Field SET SeqNo=730, AD_Reference_Value_ID=NULL, AD_Val_Rule_ID=NULL, IsToolbarButton=NULL,Updated=TO_TIMESTAMP('2017-10-14 13:38:26','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=200624
;

-- Oct 14, 2017 1:38:26 PM GST
UPDATE AD_Field SET SeqNo=740, AD_Reference_Value_ID=NULL, AD_Val_Rule_ID=NULL, IsToolbarButton=NULL,Updated=TO_TIMESTAMP('2017-10-14 13:38:26','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=4362
;

-- Oct 14, 2017 1:38:26 PM GST
UPDATE AD_Field SET SeqNo=750, AD_Reference_Value_ID=NULL, AD_Val_Rule_ID=NULL, IsToolbarButton=NULL,Updated=TO_TIMESTAMP('2017-10-14 13:38:26','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=4361
;

-- Oct 14, 2017 1:38:26 PM GST
UPDATE AD_Field SET SeqNo=760, AD_Reference_Value_ID=NULL, AD_Val_Rule_ID=NULL, IsToolbarButton=NULL,Updated=TO_TIMESTAMP('2017-10-14 13:38:26','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=6552
;

-- Oct 14, 2017 1:38:26 PM GST
UPDATE AD_Field SET SeqNo=770, AD_Reference_Value_ID=NULL, AD_Val_Rule_ID=NULL, IsToolbarButton=NULL,Updated=TO_TIMESTAMP('2017-10-14 13:38:26','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=4044
;

-- Oct 14, 2017 1:38:26 PM GST
UPDATE AD_Field SET SeqNo=780, AD_Reference_Value_ID=NULL, AD_Val_Rule_ID=NULL, IsToolbarButton=NULL,Updated=TO_TIMESTAMP('2017-10-14 13:38:26','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=4266
;

-- Oct 14, 2017 1:38:26 PM GST
UPDATE AD_Field SET SeqNo=790, AD_Reference_Value_ID=NULL, AD_Val_Rule_ID=NULL, IsToolbarButton=NULL,Updated=TO_TIMESTAMP('2017-10-14 13:38:26','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=4040
;

-- Oct 14, 2017 1:38:26 PM GST
UPDATE AD_Field SET SeqNo=800, AD_Reference_Value_ID=NULL, AD_Val_Rule_ID=NULL, IsToolbarButton=NULL,Updated=TO_TIMESTAMP('2017-10-14 13:38:26','YYYY-MM-DD HH24:MI:SS'),UpdatedBy=100 WHERE AD_Field_ID=1001000
;

